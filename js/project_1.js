var TaskFromLocalStorage = localStorage.getItem('the_task');
var tasks = [];

if (TaskFromLocalStorage != null) {
    tasks = JSON.parse(TaskFromLocalStorage);
};
drowNote();

/* function for add now task */
function addTask() {
    let theTask = document.getElementById('Task').value;
    let theDate = document.getElementById('Date').value;
    let theHour = document.getElementById('Hour').value;
    if (theTask == '') {
        alert('Note! you must to fiil task ')
    } else if (theDate == '') {
        alert('Note! you must to fiil date ')

    } else {
        let newTask = {
            task: theTask,
            Date: theDate,
            Hour: theHour
        };
        tasks.push(newTask);
        localStorage.setItem('the_task', JSON.stringify(tasks));
        drowNote();
        forTransition();
        document.getElementById('Task').value = '';
        document.getElementById('Date').value = '';
        document.getElementById('Hour').value = '';

        return false;
    }
};

/* function for drow the note */
function drowNote() {
    document.querySelector('#all_note').innerHTML = '';
    let rowNote = document.createElement('div');
    rowNote.classList.add("row", "container-fluid");
    all_note.appendChild(rowNote);

    for (let i = 0; i < tasks.length; i++) {
        let newNote = document.createElement('div');
        newNote.classList.add("col-md-3", "noted");

        let taskNote = document.createElement('div');
        taskNote.classList.add('task_note');
        taskNote.innerHTML = tasks[i].task.replace(/\n/g, "<br />");

        let time = document.createElement('div');
        time.classList.add('task_time', 'text-left', 'mb-2', 'align-bottom');
        time.innerHTML = tasks[i].Date + '<br>' + tasks[i].Hour;

        let deletButton = document.createElement('button');
        deletButton.id = 'delet_button';

        deletButton.setAttribute('onclick', 'deleteTask' + '(' + [i] + ')');
        let deleteIcon = document.createElement('i');
        deleteIcon.id = 'delete_icon';
        deleteIcon.classList.add('fas', 'fa-trash-alt');

        rowNote.appendChild(newNote);
        newNote.appendChild(deletButton);
        deletButton.appendChild(deleteIcon);
        newNote.appendChild(taskNote);
        newNote.appendChild(time);
    }
};

/* function to delete note */
function deleteTask(whoToDelete) {
    tasks.splice(whoToDelete, 1);
    localStorage.setItem('the_task', JSON.stringify(tasks));
    drowNote();
};

/* function for do transition When a new note is added */
function forTransition() {
    let allNotes = [];
    allNotes = document.querySelectorAll('.noted');
    let first = allNotes[0];
    let last = allNotes[allNotes.length - 1];
    last.classList.add('forlastNote');
};